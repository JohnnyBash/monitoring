#!/bin/bash

OK=0
WARNING=1
CRITICAL=2
UNKNOWN=3

# param1 n1
# param2 n2
function lowest
{
    local errno=0

    local n1=$1
    local n2=$2

    if [[ $n1 -le $n2 ]]
    then
        echo $n1
    else
        echo $n2
    fi

    return $errno
}

# param1 n1
# param2 n2
function greatest
{
	local errno=0

	local n1=$1
	local n2=$2

	if [[ $n1 -ge $n2 ]]
	then
		echo $n1
	else
		echo $n2
	fi

	return $errno
}

function getPs
{
	local errno=$OK

	echo "$(ps ax -o args | grep -v "$cmd")"

	return $errno
}

function main
{
	local errno=$OK
	local state=""

	local output=""
	local perfdata=""

	local psrunning
	local psname

	if [[ $errno -eq $OK ]]
	then
		psrunning="$(getPs)"
		if [[ $? -ne 0 ]]
		then
			errno=$(greatest $errno $UNKNOWN)
		fi
	fi

	if [[ $errno -eq $OK ]]
	then
		for psname in $pslist
		do
			if grep $psname > /dev/null <<< $psrunning
			then
				output=$(echo -e "$output\n$psname : OK")
			else
				output=$(echo -e "$output\n$psname : NOT RUNNING")
				errno=$(greatest $errno $CRITICAL)
			fi
		done
	fi

	output=$(sort <<< "$output")

	case $errno in
		0 ) state=$(echo "OK - $state");;
		1 ) state=$(echo "WARNING - $state");;
		2 ) state=$(echo "CRITICAL - $state");;
		* ) state=$(echo "UNKNOWN - $state");;
	esac

	echo -e "$state\n$output|$perfdata"

	return $errno
}

cmd=${0##*/}
cmd=${cmd%.*}

errno=0
warnThreshold=0
critThreshold=0
pslist=""

if [[ $errno -eq 0 ]]
then
	opts=$(getopt \
		--name "$cmd" \
		--options "w:c:p:" \
		--longoptions "warning:,critical:,psname:" \
		-- "$@"
	)
	if [[ $? -ne 0 ]]
	then
		errno=1
	fi
fi

if [[ $errno -eq 0 ]]
then
	moreopt=0
	while [[ $moreopt -eq 0 ]]
	do
		case "$1" in
			-w | --warning ) warnThreshold=$2; shift 2;;
			-c | --critical ) critThreshold=$2; shift 2;;
			-p | --psname ) pslist="$pslist$2 "; shift 2;;
			-- ) moreopt=1; shift 1;;
			* ) moreopt=1;;
		esac
	done

	warnThreshold=$(lowest $warnThreshold $critThreshold)
	critThreshold=$(greatest $warnThreshold $critThreshold)

	eval set -- ${opts##*--}
	shift $((OPTIND-1))
fi

if [[ $errno -eq 0 ]]
then
	main
	errno=$?
fi

exit $errno
