#!/bin/bash

# <output> | 'label'=value[UOM];[warn];[crit];[min];[max]

OK=0
WARNING=1
CRITICAL=2
UNKNOWN=3

# param1 n1
# param2 n2
function lowest
{
	local errno=0
	local n1=$1
	local n2=$2

	if [[ $n1 -le $n2 ]]
	then
		echo $n1
	else
		echo $n2
	fi

	return $errno
}

# param1 n1
# param2 n2
function greatest
{
	local errno=0
	local n1=$1
	local n2=$2

	if [[ $n1 -ge $n2 ]]
	then
		echo $n1
	else
		echo $n2
	fi

	return $errno
}

function getCPUInfo
{
	local errno=0
	local errnopipe

	mpstat -P ALL --dec=0 1 2 | grep "^Average" | tail -n+2 | tr -s " " | cut -d" " -f2,3,5,6,12; errnopipe=(${PIPESTATUS[*]})
	if [[ ${errnopipe[0]} -ne 0 ]]
	then
		errno=1
	fi

	return $errno
}

function getBiggestCPUUsage
{
	local errno=0
	local errnopipe

	ps ax -o pcpu,args --sort -pcpu | head -n6; errnopipe=(${PIPESTATUS[*]})
	if [[ ${errnopipe[0]} -ne 0 ]]
	then
		errno=1
	fi

	return $errno
}

function main
{
	local errno=$OK
	local state=""

	local output="#"
	local perfdata=""
	local cpusInfo
	local globalUsage
	local cpuUsage
	local cpu
	local pusr
	local psys
	local piowait
	local pidle
	local pused
	local cpuOutput
	local cpuPerfdata

	if [[ $errno -eq $OK ]]
	then
		cpuInfo=$(getCPUInfo)
		if [[ $? -ne 0 ]]
		then
			errno=$(greatest $errno $UNKNOWN)
		fi
	fi

	if [[ $errno -eq $OK ]]
	then
		output=$(echo -e "$output\n      cpu usr sys iowait")
		while read cpu pusr psys piowait pidle
		do
			pused=$((100-$pidle))

			if [[ $pused -ge $warnThreshold ]]
			then
				if [[ $pused -ge $critThreshold ]]
				then
					errno=$(greatest $errno $CRITICAL)
				else
					errno=$(greatest $errno $WARNING)
				fi
			fi

			cpuOutput=$(printf "%3s : %2d%% %2d%% %2d%% %5d%%" $cpu $pused $pusr $psys $piowait)
			cpuPerfdata="$cpu=$pused%;$warnThreshold;$critThreshold;0;100"
			
			output=$(echo -e "$output\n$cpuOutput")
			perfdata=$(echo -e "$perfdata$cpuPerfdata ")
		done <<< "$cpuInfo"
	fi

	if [[ $errno -eq $OK ]]
	then
		output=$(echo -e "$output\n$(getBiggestCPUUsage | sed -e 's/^\(.*\)$/\t\1/g'))")
	fi

	case $errno in
		0 ) state=$(echo "OK - $state");;
		1 ) state=$(echo "WARNING - $state");;
		2 ) state=$(echo "CRITICAL - $state");;
		* ) state=$(echo "UNKNOWN - $state");;
	esac

	echo -e "$state\n$output|$perfdata"

	return $errno
}

cmd=${0##*/}
cmd=${cmd%.*}

errno=0
warnThreshold=90
critThreshold=95

if [[ $errno -eq 0 ]]
then
	opts=$(getopt \
		--name "$cmd" \
		--options "w:c:" \
		--longoptions "warning:,critical:" \
		-- "$@"
	)
	if [[ $? -ne 0 ]]
	then
		errno=1
	fi
fi

if [[ $errno -eq 0 ]]
then
	moreopt=0
	while [[ $moreopt -eq 0 ]]
	do
		case "$1" in
			-w | --warning ) warnThreshold=$2; shift 2;;
			-c | --critical ) critThreshold=$2; shift 2;;
			-- ) moreopt=1; shift 1;;
			* ) moreopt=1;;
		esac
	done

	warnThreshold=$(lowest $warnThreshold $critThreshold)
	critThreshold=$(greatest $warnThreshold $critThreshold)

	eval set -- ${opts##*--}
	shift $((OPTIND-1))
fi

if [[ $errno -eq 0 ]]
then
	main
	errno=$?
fi

exit $errno

