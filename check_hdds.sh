#!/bin/bash

OK=0
WARNING=1
CRITICAL=2
UNKNOWN=3

# param1 n1
# param2 n2
function lowest
{
	local errno=0

	local n1=$1
	local n2=$2

	if [[ $n1 -le $n2 ]]
	then
		echo $n1
	else
		echo $n2
	fi

	return $errno
}

# param1 n1
# param2 n2
function greatest
{
	local errno=0

	local n1=$1
	local n2=$2

	if [[ $n1 -ge $n2 ]]
	then
		echo $n1
	else
		echo $n2
	fi

	return $errno
}

function getHDDs
{
	local errno=0
	local errnopipe

	df -h | grep "^/dev"; errnopipe=(${PIPESTATUS[*]})

	if [[ ${errnopipe[0]} -ne 0 ]] || [[ ${errnopipe[1]} -ne 0 ]]
	then
		errno=1
	fi

	return $errno
}

# param1 file
function getBiggestDir
{
	local errno=0
	local errnopipe

	local file=$1

	du -hS $file 2> /dev/null | sort -hr 2> /dev/null | head -n5; errnopipe=(${PIPESTATUS[*]})

	if [[ ${errnopipe[0]} -ne 0 ]]
	then
		errno=1
	fi


	return $errno
}

function main
{
	local errno=$OK
	local state=""

	local output="#"
	local perfdata=""

	local nUseperc
	local hddPerfdata
	local fileSystem
	local size
	local used
	local avail
	local useperc
	local mountedOn

	if [[ $errno -eq $OK ]]
	then
		hdds=$(getHDDs)
		if [[ $? -ne 0 ]]
		then
			errno=$(greatest $errno $UNKNOWN)
		fi
	fi

	if [[ $errno -eq $OK ]]
	then
		while read fileSystem size used avail useperc mountedOn
		do
			nUseperc=${useperc%%%*}

			if [[ $nUseperc -ge $warnThreshold ]]
			then
				if [[ $nUseperc -ge $critThreshold ]]
				then
					errno=$(greatest $errno $CRITICAL)
				else
					errno=$(greatest $errno $WARNING)
				fi
			fi

			hddOutput=$(echo -e "$mountedOn : $used/$size ($useperc)\n$(getBiggestDir $mountedOn | sed -e 's/^\(.*\)$/\t\1/g')")
			# <output> | 'label'=value[UOM];[warn];[crit];[min];[max]
			hddPerfdata="$(basename $fileSystem)=$nUseperc%;$warnThreshold;$critThreshold;0;100"

			output=$(echo -e "$output\n$hddOutput")
			perfdata=$(echo -e "$perfdata$hddPerfdata ")
		done <<< "$hdds"
	fi

	case $errno in
		0 ) state=$(echo "OK - $state");;
		1 ) state=$(echo "WARNING - $state");;
		2 ) state=$(echo "CRITICAL - $state");;
		* ) state=$(echo "UNKNOWN - $state");;
	esac

	echo -e "$state\n$output|$perfdata"

	return $errno
}

cmd=${0##*/}
cmd=${cmd%.*}

errno=0
warnThreshold=90
critThreshold=95

if [[ $errno -eq 0 ]]
then
	opts=$(getopt \
		--name "$cmd" \
		--options "w:c:" \
		--longoptions "warning:,critical:" \
		-- "$@"
	)
	if [[ $? -ne 0 ]]
	then
		errno=1
	fi
fi

if [[ $errno -eq 0 ]]
then
	moreopt=0
	while [[ $moreopt -eq 0 ]]
	do
		case "$1" in
			-w | --warning ) warnThreshold=$2; shift 2;;
			-c | --critical ) critThreshold=$2; shift 2;;
			-- ) moreopt=1; shift 1;;
			* ) moreopt=1;;
		esac
	done

	warnThreshold=$(lowest $warnThreshold $critThreshold)
	critThreshold=$(greatest $warnThreshold $critThreshold)

	eval set -- ${opts##*--}
	shift $((OPTIND-1))
fi

if [[ $errno -eq 0 ]]
then
	main
	errno=$?
fi

exit $errno
