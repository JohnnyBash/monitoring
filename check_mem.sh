#!/bin/bash

# <output> | 'label'=value[UOM];[warn];[crit];[min];[max]

OK=0
WARNING=1
CRITICAL=2
UNKNOWN=3

# param1 n1
# param2 n2
function lowest
{
	local errno=0
	local n1=$1
	local n2=$2

	if [[ $n1 -le $n2 ]]
	then
		echo $n1
	else
		echo $n2
	fi

	return $errno
}

# param1 n1
# param2 n2
function greatest
{
	local errno=0
	local n1=$1
	local n2=$2

	if [[ $n1 -ge $n2 ]]
	then
		echo $n1
	else
		echo $n2
	fi

	return $errno
}

function getMemInfo
{
	local errno=0
	local errnopipe

	free -b 2> /dev/null | tr -d : | tr -s " " | cut -d" " -f1-3,7; errnopipe=(${PIPESTATUS[*]})
	if [[ ${errnopipe[0]} -ne 0 ]]
	then
		errno=1
	fi

	return $errno
}

function getBiggestMemUsage
{
	local errno=0
	local errnopipe

	ps ax -o pmem,args --sort -pmem | head -n6; errnopipe=(${PIPESTATUS[*]})
	if [[ ${errnopipe[0]} -ne 0 ]]
	then
		errno=1
	fi

	return $errno
}

function main
{
	local errno=$OK
	local state=""
	local output="#"
	local perfdata=""
	local memInfo
	local memUsage
	local swapUsage
	local type
	local total
	local used
	local available
	local pused
	local memOutput
	local memPerfdata

	if [[ $errno -eq $OK ]]
	then
		memInfo=$(getMemInfo)
		if [[ $? -eq 0 ]]
		then
			memUsage=$( grep "^Mem" <<< $memInfo)
			swapUsage=$( grep "^Swap" <<< $memInfo)
		else
			errno=$(greatest $errno $UNKNOWN)
		fi
	fi

	if [[ $errno -eq $OK ]]
	then
		# Processing memory
		read type total used available <<< "$memUsage"
		used=$((total-available))
		pused=$((used*100/total))

		if [[ $pused -ge $warnThreshold ]]
		then
			if [[ $pused -ge $critThreshold ]]
			then
				errno=$(greatest $errno $CRITICAL)
			else
				errno=$(greatest $errno $WARNING)
			fi
		fi

		memOutput="$type : $(numfmt --to=iec $used)/$(numfmt --to=iec $total) ($pused%)"
		memPerfdata="$type=$pused;$warnThreshold;$critThreshold;0;100"
		output=$(echo -e "$output\n$memOutput")
		perfdata=$(echo -e "$perfdata$memPerfdata ")

		# Processing swap memory
		read type total used available <<< "$swapUsage"
		pused=$((used*100/total))

		if [[ $pused -ge $warnThreshold ]]
		then
			if [[ $pused -ge $critThreshold ]]
			then
				errno=$(greatest $errno $CRITICAL)
			else
				errno=$(greatest $errno $WARNING)
			fi
		fi

		memOutput="$type : $(numfmt --to=iec $used)/$(numfmt --to=iec $total) ($pused%)"
		memPerfdata="$type=$pused%;$warnThreshold;$critThreshold;0;100"
		output=$(echo -e "$output\n$memOutput")
		perfdata=$(echo -e "$perfdata$memPerfdata ")
	fi

	if [[ $errno -eq $OK ]]
	then
		output=$(echo -e "$output\n$(getBiggestMemUsage | sed -e 's/^\(.*\)$/\t\1/g'))")
	fi

	case $errno in
		0 ) state=$(echo "OK - $state");;
		1 ) state=$(echo "WARNING - $state");;
		2 ) state=$(echo "CRITICAL - $state");;
		* ) state=$(echo "UNKNOWN - $state");;
	esac

	echo -e "$state\n$output|$perfdata"

	return $errno
}

cmd=${0##*/}
cmd=${cmd%.*}

errno=0
warnThreshold=90
critThreshold=95

if [[ $errno -eq 0 ]]
then
	opts=$(getopt \
		--name "$cmd" \
		--options "w:c:" \
		--longoptions "warning:,critical:" \
		-- "$@"
	)
	if [[ $? -ne 0 ]]
	then
		errno=1
	fi
fi

if [[ $errno -eq 0 ]]
then
	moreopt=0
	while [[ $moreopt -eq 0 ]]
	do
		case "$1" in
			-w | --warning ) warnThreshold=$2; shift 2;;
			-c | --critical ) critThreshold=$2; shift 2;;
			-- ) moreopt=1; shift 1;;
			* ) moreopt=1;;
		esac
	done

	warnThreshold=$(lowest $warnThreshold $critThreshold)
	critThreshold=$(greatest $warnThreshold $critThreshold)

	eval set -- ${opts##*--}
	shift $((OPTIND-1))
fi

if [[ $errno -eq 0 ]]
then
	main
	errno=$?
fi

exit $errno

